let carYearArray = [] ;

function problem4(inventory) {
    if(inventory == null){
        return [];
    }
    if(typeof(inventory)=='object'){
        for(i = 0; i<inventory.length ; i++){
            carYearArray[i] = inventory[i].car_year;
        }
        return carYearArray;
    }
    return [];
}

module.exports = problem4;