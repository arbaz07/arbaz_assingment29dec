
function problem1(inventory, id){
    if (inventory== null || id == null){
        return [];
    }
   
    if(inventory instanceof Array){
        for(i=0; i<inventory.length; i++){
            
            if(inventory[i].id==id){
                //return "Car "+id+" is a "+inventory[i].car_year+" "+inventory[i].car_make+" "+inventory[i].car_model;
                return inventory[i];
            }
        }
    }
    if(inventory instanceof Object){
        if(inventory.id == id &&typeof(inventory.car_make) != "undefined"  && typeof(inventory.car_model) != "undefined"  && typeof(inventory.car_year) != "undefined" ){
            //return "Car "+id+" is a "+inventory.car_year+" "+inventory.car_make+" "+inventory.car_model;
            return inventory;
        }else{
            return [];
        }
    }
    return [];
}

module.exports = problem1;