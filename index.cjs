const problem1 = require("./problem1.cjs");
const problem_two = require("./problem2.cjs");
const problem_three = require("./problem3.cjs");
const problem_four = require("./problem4.cjs");
const problem_five = require("./problem5.cjs");
const problem_six = require("./problem6.cjs");
const inventory = require("./data.cjs");



//Question One
console.log(problem1({id: 33, name: "Test", length: 10}, 33));



//Question Two
let answer2 = problem_two(inventory);
console.log(answer2);


//Question Three
let answer3 = problem_three(inventory);
for(i=0; i<answer3.length; i++){
    console.log(answer3[i]);
}


//Question Four
let answer4 = problem_four(inventory);
console.log(answer4);


// let count=0;
// for(i=0; i<answer4.length; i++){
//     if(answer4[i] < 2000){
//         count++;
//     }
// }
// console.log(count);


// Question Five
let answer5=problem_five(answer4);
console.log(answer5);


//Question Six
let answer6 = problem_six(inventory,"BMW", "Audi");
console.log(answer6);