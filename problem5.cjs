
let count=0;

function problem5(years, year ){
    if(years == null || year==null){
        return [];
    }
    if(Array.isArray(years)){
        for(i=0; i<years.length; i++){
            if(years[i] < year){
                count++;
            }
        }
        return count;
    }
    return [];
}

module.exports = problem5;