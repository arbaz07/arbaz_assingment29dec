
function sortByModelName(inventory){
    if(inventory == null){
        return [];
    }
    if(typeof(inventory) == 'object'){
        inventory.sort((a,b) => {
            const modelA = a.car_model.toUpperCase();
            const modelB = b.car_model.toUpperCase();
            if(modelA < modelB){
                return -1;
            }
            if(modelA > modelB){
                return 1;
            }
            return 0;
        
        });
        return inventory;
    }
    return [];
}


module.exports = sortByModelName;