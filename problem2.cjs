
function lastCarInInventory(inventory){
    if(inventory == null){
        return [];
    }
    if(typeof(inventory)=='object'){
        let lastCarInInventory =inventory[inventory.length-1];
        return lastCarInInventory;
        //return "Last car is a "+lastCarInInventory.car_make+" "+lastCarInInventory.car_model;
    }
    return [];
}

module.exports = lastCarInInventory;